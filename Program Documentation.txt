Program Created: 
Initially created in 2012, later adapted to work with SAP.

Created By:
Shen Yu

Program Used By: 
Rework, Engineering, Quality

Program's Purpose: 
Used to reprint power and frame labels.Pulls data from CA01S0015 about panel wattage and other properties.

Deployed:
Supervisors office computer.

Referneces:
None.


