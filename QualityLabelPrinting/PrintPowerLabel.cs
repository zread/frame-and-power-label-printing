﻿/*
 * Created by SharpDevelop.
 * User: shen.yu
 * Date: 3/30/2012
 * Time: 10:34 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using LabelManager2;
using System.Collections;
using System.Diagnostics;
using 条码打印测试;

namespace QualityLabelPrinting
{
	/// <summary>
	/// Description of PrintLabel.
	/// </summary>
	public class PrintPowerLabel
	{
		public PrintPowerLabel()
		{
		}
		
		public static string print(PowerLabel lb, string labFilePath)
		{
			LabelManager2.ApplicationClass lbl = new ApplicationClass();
        	try
        	{
				lbl.Documents.Open(labFilePath, false);// 调用设计好的label文件
				Document doc = lbl.ActiveDocument;
				int VarCnt = lbl.ActiveDocument.Variables.Count;

				for (int i = 1; i <= VarCnt; i++)
				{
					if (lbl.ActiveDocument.Variables.Item(i).Name.ToLower() == "model")
					{
						lbl.ActiveDocument.Variables.Item(i).Value = lb.model;
						continue;
					}
					
					if (lbl.ActiveDocument.Variables.Item(i).Name.ToLower() == "pmpp")
					{
						lbl.ActiveDocument.Variables.Item(i).Value = lb.pmpp;
						continue;
					}
					
					if (lbl.ActiveDocument.Variables.Item(i).Name.ToLower() == "vmpp")
					{
						lbl.ActiveDocument.Variables.Item(i).Value = lb.vmpp;
						continue;
					}
					
					if (lbl.ActiveDocument.Variables.Item(i).Name.ToLower() == "impp")
					{
						lbl.ActiveDocument.Variables.Item(i).Value = lb.impp;
						continue;
					}					

					if (lbl.ActiveDocument.Variables.Item(i).Name.ToLower() == "voc")
					{
						lbl.ActiveDocument.Variables.Item(i).Value = lb.voc;
						continue;
					}
					
					if (lbl.ActiveDocument.Variables.Item(i).Name.ToLower() == "isc")
					{
						lbl.ActiveDocument.Variables.Item(i).Value = lb.isc;
						continue;
					}

					if (lbl.ActiveDocument.Variables.Item(i).Name.ToLower() == "sn")
					{
						lbl.ActiveDocument.Variables.Item(i).Value = lb.sn;
						continue;
					}
					
					if (lbl.ActiveDocument.Variables.Item(i).Name.ToLower() == "moduleclass")
					{
						lbl.ActiveDocument.Variables.Item(i).Value = lb.moduleclass;
						continue;
					}						
				}
				doc.PrintLabel(1, 1, 1, 1); //打印数量
				doc.PrintDocument(0);
				lbl.Quit();  //退出
				Process[] killbyname = Process.GetProcessesByName("lppa");
				for (int i = 0; i < killbyname.Length; i++)
				{
					killbyname[i].Kill();
				}
        	}
       		catch (Exception ex)
            {
                return ex.Message;
            }
       		return "";
		}
		
		//print label using ZPL template
		
		public static bool print2(PowerLabel lb, string ZPL, string printer)
		{
			ZPL = ZPL.Replace("^FDmodel^FS","^FD"+lb.model+"^FS");
			ZPL = ZPL.Replace("^FDpmpp^FS","^FD"+lb.pmpp+"^FS");
			ZPL = ZPL.Replace("^FDvmpp^FS","^FD"+lb.vmpp+"^FS");
			ZPL = ZPL.Replace("^FDimpp^FS","^FD"+lb.impp+"^FS");
			ZPL = ZPL.Replace("^FDvoc^FS","^FD"+lb.voc+"^FS");
			ZPL = ZPL.Replace("^FDisc^FS","^FD"+lb.isc+"^FS");
			ZPL = ZPL.Replace("^FDsn^FS","^FD"+lb.sn+"^FS");
			ZPL = ZPL.Replace("^FDmoduleclass^FS","^FD"+lb.moduleclass+"^FS");
			ZPL = ZPL.Replace("^FDvoltage^FS","^FD"+lb.voltage+"^FS");
			ZPL = ZPL.Replace("^FDtime^FS", "^FD"+DateTime.Now.ToShortDateString()+"^FS");
			
			return RawPrinterHelper.SendStringToPrinter(printer, ZPL, "");
		}
	}
}
