﻿/*
 * Created by SharpDevelop.
 * User: shen.yu
 * Date: 3/30/2012
 * Time: 10:34 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using LabelManager2;

namespace QualityLabelPrinting
{
	/// <summary>
	/// Description of PrintLabel.
	/// </summary>
	public class PrintLabel
	{
		public PrintLabel()
		{
		}
		
		public static string print(Label lb, string labFilePath)
		{
			LabelManager2.ApplicationClass lbl = new ApplicationClass();
        	try
        	{
				lbl.Documents.Open(labFilePath, false);// 调用设计好的label文件
				Document doc = lbl.ActiveDocument;
				int VarCnt = lbl.ActiveDocument.Variables.Count;

				for (int i = 1; i <= VarCnt; i++)
				{
					if (lbl.ActiveDocument.Variables.Item(i).Name.ToLower() == "model")
					{
						lbl.ActiveDocument.Variables.Item(i).Value = lb.model;
						continue;
					}
					
					if (lbl.ActiveDocument.Variables.Item(i).Name.ToLower() == "pmpp")
					{
						lbl.ActiveDocument.Variables.Item(i).Value = lb.pmpp;
						continue;
					}
					
					if (lbl.ActiveDocument.Variables.Item(i).Name.ToLower() == "vmpp")
					{
						lbl.ActiveDocument.Variables.Item(i).Value = lb.vmpp;
						continue;
					}
					
					if (lbl.ActiveDocument.Variables.Item(i).Name.ToLower() == "impp")
					{
						lbl.ActiveDocument.Variables.Item(i).Value = lb.impp;
						continue;
					}					

					if (lbl.ActiveDocument.Variables.Item(i).Name.ToLower() == "voc")
					{
						lbl.ActiveDocument.Variables.Item(i).Value = lb.voc;
						continue;
					}
					
					if (lbl.ActiveDocument.Variables.Item(i).Name.ToLower() == "isc")
					{
						lbl.ActiveDocument.Variables.Item(i).Value = lb.isc;
						continue;
					}

					if (lbl.ActiveDocument.Variables.Item(i).Name.ToLower() == "sn")
					{
						lbl.ActiveDocument.Variables.Item(i).Value = lb.sn;
						continue;
					}
					
					if (lbl.ActiveDocument.Variables.Item(i).Name.ToLower() == "moduleclass")
					{
						lbl.ActiveDocument.Variables.Item(i).Value = lb.moduleclass;
						continue;
					}						
				}
				doc.PrintLabel(1, 1, 1, 1); //打印数量
				doc.PrintDocument(0);
				lbl.Quit();  //退出
        	}
       		catch (Exception ex)
            {
                return ex.Message;
            }
       		return "";
		}
	}
}
