﻿using System;
using System.Runtime.InteropServices;

namespace 条码打印测试
{
    public class RawPrinterHelper
    {
        #region Windows API定义

        /// <summary>
        /// API结构定义
        /// </summary>
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public class DocInfoA
        {
            [MarshalAs(UnmanagedType.LPStr)]
            public string PDocName;
            [MarshalAs(UnmanagedType.LPStr)]
            public string POutputFile;
            [MarshalAs(UnmanagedType.LPStr)]
            public string PDataType;
        }

        [DllImport("winspool.Drv", EntryPoint = "OpenPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool OpenPrinter([MarshalAs(UnmanagedType.LPStr)] string szPrinter, out IntPtr hPrinter, IntPtr pd);

        [DllImport("winspool.Drv", EntryPoint = "ClosePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool ClosePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "StartDocPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool StartDocPrinter(IntPtr hPrinter, Int32 level, [In, MarshalAs(UnmanagedType.LPStruct)] DocInfoA di);

        [DllImport("winspool.Drv", EntryPoint = "EndDocPrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EndDocPrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "StartPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool StartPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "EndPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EndPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "WritePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool WritePrinter(IntPtr hPrinter, IntPtr pBytes, Int32 dwCount, out Int32 dwWritten);

        #endregion

        #region 打印方法

        /// <summary>
        /// 发送字节数组到打印机
        /// </summary>
        /// <param name="szPrinterName">打印机的名字</param>
        /// <param name="pBytes">要打印的非托管字节数组</param>
        /// <param name="dwCount">数组的长度</param>
        /// <param name="docTitle"></param>
        /// <returns>是否成功</returns>
        public static bool SendBytesToPrinter(string szPrinterName, IntPtr pBytes, Int32 dwCount, string docTitle)
        {
            IntPtr hPrinter;
            var di = new DocInfoA();
            var bSuccess = false;

            di.PDocName = "CSIMES RAW Document Title : " + docTitle + " Time : " +
                          DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
            di.PDataType = "RAW";

            //打开打印机
            if (OpenPrinter(szPrinterName.Normalize(), out hPrinter, IntPtr.Zero))
            {
                //开始一个文档
                if (StartDocPrinter(hPrinter, 1, di))
                {
                    //开始一页
                    if (StartPagePrinter(hPrinter))
                    {
                        //打印
                        int dwWritten;
                        bSuccess = WritePrinter(hPrinter, pBytes, dwCount, out dwWritten);
                        EndPagePrinter(hPrinter);
                    }
                    EndDocPrinter(hPrinter);
                }
                ClosePrinter(hPrinter);
            }

            return bSuccess;
        }

        /// <summary>
        /// 发送字符串到打印机
        /// </summary>
        /// <param name="szPrinterName">打印机的名字</param>
        /// <param name="szString">要打印的字符串</param>
        /// <param name="docTitle">打印的文档标题字符串</param>
        /// <returns>是否成功</returns>
        public static bool SendStringToPrinter(string szPrinterName, string szString, string docTitle)
        {
            //字符串的长度
            var dwCount = szString.Length;
            // Assume that the printer is expecting ANSI text, and then convert
            // the string to ANSI text.
            var pBytes = Marshal.StringToCoTaskMemAnsi(szString);
            // Send the converted ANSI string to the printer.
            var retValue = SendBytesToPrinter(szPrinterName, pBytes, dwCount, docTitle);
            Marshal.FreeCoTaskMem(pBytes);
            return retValue;
        }

        #endregion
    }
}