﻿/*
 * Created by SharpDevelop.
 * User: shen.yu
 * Date: 3/30/2012
 * Time: 10:10 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;
using System.Xml;

namespace QualityLabelPrinting
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			initializeZPL();//read ZPL template and printer name from config.xml
			initializeZPL1();
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		private string whiteLabelPath = Application.StartupPath + "\\whitelabel.lab";
		private string cstring = ReadXML("DBinfo");
		private string ZPL,ZPL1;
		private string printer,printer1;
		
		
		
			
		private static string ReadXML(string node)
		{
			string xmlFile = Application.StartupPath + "\\Config.xml";
			XmlReader reader = XmlReader.Create(xmlFile);
			reader.ReadToFollowing(node);
			string info = reader.ReadElementContentAsString();
			return info;
		}
		
		void TbSNKeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == '\r')
            {
                string strTemp = tbSN.Text;
                string[] sDataSet = strTemp.Split('\n');
                for (int i = 0; i < sDataSet.Length; i++)
                {
                    sDataSet[i] = sDataSet[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
                    if (sDataSet[i] != null && sDataSet[i] != "")
                        printPowerLabel(sDataSet[i]);
                }
                tbSN.Clear();
            }
		}
			
		void printPowerLabel(string sn)
		{
			if (sn.Length != 13 && sn.Length != 14)
            {
				MessageBox.Show("Printing "+sn+" fail. Incorrect SN length.");
                return;
            }
			
			PowerLabel lb = new PowerLabel();
			lb.sn = sn;
			
			//Removed by Zach Read November 5, 2015
//			string sql = "select moduleclass from qualityjudge where id = (select max(ID) from qualityjudge where sn = '"+sn+"')";
//			using (SqlConnection con = new SqlConnection(cstring))
//			{
//				con.Open();
//				using (SqlCommand command = new SqlCommand(sql, con))
//	        	using (SqlDataReader reader = command.ExecuteReader())
//	        	{			    		
//		    		if(reader.Read() && reader != null)
//	        		{
//		    			lb.moduleclass = reader.GetString(0);
//		    			if(lb.moduleclass == "A-" || lb.moduleclass == "RW" || lb.moduleclass == "A3")
//		    			{
//		    				MessageBox.Show("Printing fail. It is class "+lb.moduleclass+"!");
//		    				return;
//		    			}
//		    			else if(lb.moduleclass == "A")
//		    			{
//		    				lb.moduleclass = "";
//		    			}
//	        		}
//		    		else
//		    		{
//		    			lb.moduleclass = "";
//		    		}
//	        	}
//			}
			
			//This query gets the model type, power and material code of the serial number
			string celltype = "";
			//string sql = "select product.modeltype, nominalpower, product.materialcode from newtemptable inner join product on newtemptable.elecid = product.elecid where newtemptable.elecid = (select MAX(elecid) from Product where SN = '"+sn+"')";
			string sql = "select P.modeltype, T.Pmax from Product as P inner join ElecParaTest as T on P.SN = T.FNumber where p.interid = (select max(interid) from product where SN = '"+sn+"')";
			using (SqlConnection con = new SqlConnection(cstring))
			{
				con.Open();
				using (SqlCommand command = new SqlCommand(sql, con))
	        	using (SqlDataReader reader = command.ExecuteReader())
	        	{			    		
		    		if(reader.Read() && reader != null)
	        		{
		    			lb.model = reader.GetString(0);
		    			double pmax = Convert.ToDouble(reader.GetValue(1));
		    			lb.pmpp = getIdealPower(pmax,sn);
		    			//lb.pmpp = reader.GetInt32(1).ToString();
		    			
		    			//Added This condition for DG modules to add the -PG on the end of the model type
		    			if (lb.model == "CS6K-P")		    			
		    				lb.model = lb.model.Substring(0,5) + lb.pmpp + lb.model.Substring(5,1);		    			
		    			else if(lb.model.Length  == 7 )
		    				lb.model = lb.model.Substring(0,5) + lb.pmpp + lb.model.Substring(5,2);
		    			else 
		    				lb.model = lb.model.Substring(0,5) + lb.pmpp + lb.model.Substring(5,1);
		    		#region removed by jacky SAP no material code	
//		    			string materialcode = reader.GetString(2);
//		    			if (materialcode.Substring(15,1) == "A" || materialcode.Substring(15,1) == "B"  || materialcode.Substring(15,1) == "C" || materialcode.Substring(15,1) == "D")
//		    			    {
//		    			    	lb.voltage = "1000";
//		    			    }
//		    			else
//	    			    {
//	    			    	if(Int32.Parse(materialcode.Substring(15,1)) < 6)
//				        	{
//				        		lb.voltage = "600";
//				        	}
//				        	else
//				        	{
//				        		lb.voltage = "1000";
//				        	}
//	    			    	
//	    			    }
//		    			
//		    			celltype = materialcode.Substring(9,1);
					#endregion
		    			
	        		}
		    		else
		    		{
		    			MessageBox.Show("Printing "+sn+" fail. No test results");
		    			return;
		    		}
	        	}
				
				
			}
			
			#region add lb.volatage celltype
			
			if(sn.Length == 14){			
				string PO = GetPONumber(sn);
				
				
				string sequence = PO.Substring(6,3);
				string Line = PO.Substring(1,1);
	        	
	        	
				sql = @"select top 1 System_Voltage, Cell_Type from ProductionOrder_Mixing_Rule where ProductionOrder like '{0}' ";
				string syr = sn.Substring(1,2);
	        	//string sMth  = DateTime.Now.Month.ToString().PadLeft(2,'0'); 
	        	sql = string.Format(sql,"C"+ Line + syr + "__" + sequence );
	        	SqlDataReader rdPO = GetDataReader(sql);
	        	if(rdPO!=null&&rdPO.Read())
	        	{
	        		lb.voltage = rdPO.GetString(0);
	        		celltype = rdPO.GetString(1);
	        	}
	        	else
	        	{
	        		MessageBox.Show("Please maintain the PO Mixing Table!");
	        		return;
	        	}
			}
			else{
				lb.voltage = "1000";
	        	celltype = "P";
			}
            #endregion
            // sql = "select  [VPPValue],[CPPValue],[OCVValue],[SCCValue] from ModelType where modeltypeid '" + lb.model + "'"; //and celltype = '"+celltype+"'" ;

            sql = "select  [VPPValue],[CPPValue],[OCVValue],[SCCValue] from ModelType where modeltypeid like '" + lb.model.Substring(0, 8) + "%'"; //and celltype = '"+celltype+"'" ;
			using (SqlConnection con = new SqlConnection(cstring))
			{
				con.Open();
				using (SqlCommand command = new SqlCommand(sql, con))
	        	using (SqlDataReader reader = command.ExecuteReader())
	        	{			    		
		    		if(reader.Read() && reader != null)
	        		{
						lb.vmpp = reader.GetString(0);
						lb.impp = reader.GetString(1);
						lb.voc = reader.GetString(2);
						lb.isc = reader.GetString(3);
                     
                    }
		    		else
		    		{
		    			MessageBox.Show("Printing fail. Power out of range.");
		    		//	return;
		    		}
	        	}
			}

			
			PrintPowerLabel.print2(lb, ZPL, printer);
		}
		
		void TbSN2KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == '\r')
            {
                string strTemp = tbSN2.Text;
                string[] sDataSet = strTemp.Split('\n');
                for (int i = 0; i < sDataSet.Length; i++)
                {
                    sDataSet[i] = sDataSet[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
                    if (sDataSet[i] != null && sDataSet[i] != "")
                        printWhiteLabel(sDataSet[i]);
                }
                tbSN2.Clear();
            }
		}
		
		void printWhiteLabel(string sn)
		{
			if (sn.Length != 13 && sn.Length != 14)
            {
				MessageBox.Show("Printing fail. Incorrect SN length.");
                return;
            }
			
			Whitelabel lb = new Whitelabel();
			lb.sn = sn;

			string nominalpower = "";
			string sql =  "select p.modeltype, T.Pmax,p.celltype, p.cellcolor,p.cellspec from Product as P INNER JOIN ElecParaTest as T on P.SN = T.FNumber  where p.interid = (select max(interid) from product where SN = '"+sn+"')";

			using (SqlConnection con = new SqlConnection(cstring))
			{
				con.Open();
				using (SqlCommand command = new SqlCommand(sql, con))
	        	using (SqlDataReader reader = command.ExecuteReader())
	        	{			    		
		    		if(reader.Read() && reader != null)
	        		{
		    			lb.model = reader.GetString(0);
		    			
		    			//nominalpower = reader.GetInt32(1).ToString();	
						double pmax = Convert.ToDouble(reader.GetValue(1));
		    			nominalpower = getIdealPower(pmax,sn);	
		    			
		    			lb.model = lb.model.Substring(0,5) + nominalpower + lb.model.Substring(5,lb.model.Length - 5);	
		    			lb.type = reader.GetString(2);
		    			lb.color = reader.GetString(3);
		    			lb.size = reader.GetString(4);
	        		}
		    		else
		    		{
		    			MessageBox.Show("Printing fail. No test results");
		    			return;
		    		}
	        	}
			}
			
			//string p = PrintWhiteLabel.print(lb, whiteLabelPath);
			PrintWhiteLabel.print3(lb,ZPL1,printer1);
		}
		
		void initializeZPL()
		{
			string xmlFile = Application.StartupPath + "\\Config.xml";
			XmlReader reader = XmlReader.Create(xmlFile);
			reader.ReadToFollowing("ZPL");
			ZPL = reader.ReadElementContentAsString();
			reader.ReadToFollowing("Printer");
			printer = reader.ReadElementContentAsString();
		}
			void initializeZPL1()
		{
			string xmlFile = Application.StartupPath + "\\FrameConfig.xml";
			XmlReader reader = XmlReader.Create(xmlFile);
			reader.ReadToFollowing("ZPL");
			ZPL1 = reader.ReadElementContentAsString();
			reader.ReadToFollowing("Printer");
			printer1 = reader.ReadElementContentAsString();
		}
		
		  private SqlDataReader GetDataReader(string SqlStr,string SqlConn="")
        {
            SqlDataReader dr = null;
            if (string.IsNullOrEmpty(SqlConn))
                SqlConn = cstring;
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(SqlConn);
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {                  
                    try
                    {
                        cmd.CommandText = SqlStr;
                        cmd.CommandType = CommandType.Text;

                        if (conn.State == ConnectionState.Closed)
                            conn.Open();
                        dr = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
                    }
                    catch (Exception ex)
                    {
                        System.Windows.Forms.MessageBox.Show(ex.Message, "Exception Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        dr = null;
                    }
             }
            return dr;
        }
		  
		  private string getIdealPower(double actualPoser, string sModuleNo, int SchemeInterID = 0, string sqlConn = "") //Modify by Gengao 2012-09-05
        {
            string idealPower = "-1"; //Modify by Gengao 2012-09-05
            string sqlGetPmaxID = "";
            if (SchemeInterID == 0) //Modify by Gengao 2012-09-05
                sqlGetPmaxID = "select a.Pmax,a.MinValue,a.CtlValue,a.MaxValue FROM SchemeEntry as a where a.PmaxID=(select ItemPmaxID from Scheme where InterID=(select distinct SchemeInterID from ElecParaTest where FNumber='" + sModuleNo + "' and InterID in (select max(InterID) from ElecParaTest where FNumber='" + sModuleNo + "')))";
            else
                sqlGetPmaxID = "select a.Pmax,a.MinValue,a.CtlValue,a.MaxValue FROM SchemeEntry as a where a.PmaxID=(select ItemPmaxID from Scheme where InterID=" + SchemeInterID + ")";
            	// taken off for canada edited by jacky 2016-01-19
            	//sqlGetPmaxID = "select a.Pmax,a.MinValue,a.CtlValue,a.MaxValue,a.RESV1
            string strPmax = "", strMinCtlValue = "", strMaxCtlValue = "", strCtlValue = "", strMinValue = "", strMaxValue = "";

            string strGrade = ""; // added by Gengao 2012-09-05
    
            
            SqlDataReader dr = null;
            try
            {
                dr = GetDataReader(sqlGetPmaxID, sqlConn);
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        strPmax = dr.GetString(0).Replace("W", "");
                        //strGrade = dr.GetString(4); edited by jacky 2016-01-19 
                        if (!dr.GetString(1).Trim().Equals(""))
                        {
                            strMinCtlValue = dr.GetString(1).Substring(dr.GetString(1).IndexOf(']') + 1, 2).Trim();
                            strMinValue = dr.GetString(1).Substring(dr.GetString(1).IndexOf(strMinCtlValue) + 2).Trim();
                        }
                        if (!dr.GetString(3).Trim().Equals(""))
                        {
                            strMaxCtlValue = dr.GetString(3).Substring(dr.GetString(3).IndexOf(']') + 1, 2).Trim();
                            strMaxValue = dr.GetString(3).Substring(dr.GetString(3).IndexOf(strMaxCtlValue) + 2).Trim();
                        }
                        strCtlValue = dr.GetString(2).Trim();
                       if (CheckIdealPower(actualPoser.ToString(), strMinCtlValue, strMaxCtlValue, strCtlValue, strMinValue, strMaxValue))
                        {
                            //Modify by Gengao 2012-09-05
                            if (strGrade.ToString().Trim().ToUpper() == "")
                                idealPower = strPmax;
                            
                                //idealPower = strPmax + "_"+ strGrade; edited by jacky 2016-01-19
                            break;
                        }
                        strPmax = ""; strMinCtlValue = ""; strMaxCtlValue = ""; strCtlValue = ""; strMinValue = ""; strMaxValue = "";
                        strGrade = "";//added by Gengao 2012-09-05
                    }
                    dr.Close();
                    dr = null;
                }
                string sqlGetDownGrade = "select Top 1 StdPower from ModuleDownGrade where sn = '{0}' order by interid desc";
                sqlGetDownGrade = string.Format(sqlGetDownGrade, sModuleNo);
                DataTable dt = GetDataTable1(sqlGetDownGrade);
                if(dt != null && dt.Rows.Count > 0)
                {
                    idealPower = dt.Rows[0][0].ToString();
                }
            }
            catch (Exception ex)
            {
                dr.Close();
                dr = null;
                MessageBox.Show("Get Nominal Power Fail! \nException:"+ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return idealPower;
        }
		  
		  private bool CheckIdealPower(string strPmax, string strMinCtlValue, string strMaxCtlValue, string strCtlValue, string strMinValue, string strMaxValue)
        {
            bool idearPower = false;
            bool MinCheck = false, MaxCheck=false;

            if (!strMinValue.Equals(""))
            {
                if (strMinCtlValue.Equals("="))
                {
                    if (Convert.ToDouble(strPmax) == Convert.ToDouble(strMinValue))
                        MinCheck = true;
                    else
                        MinCheck = false;
                }
                else if (strMinCtlValue.Equals(">"))
                {
                    if (Convert.ToDouble(strPmax) > Convert.ToDouble(strMinValue))
                        MinCheck = true;
                    else
                        MinCheck = false;
                }
                else
                {
                    if (Convert.ToDouble(strPmax) >= Convert.ToDouble(strMinValue))
                        MinCheck = true;
                    else
                        MinCheck = false;
                }
            }
            else
                MinCheck = false;

            if (!strMaxValue.Equals(""))
            {
                if (strMaxCtlValue.Equals("="))
                {
                    if (Convert.ToDouble(strPmax) == Convert.ToDouble(strMaxValue))
                        MaxCheck = true;
                    else
                        MaxCheck = false;
                }
                else if (strMaxCtlValue.Equals("<"))
                {
                    if (Convert.ToDouble(strPmax) < Convert.ToDouble(strMaxValue))
                        MaxCheck = true;
                    else
                        MaxCheck = false;
                }
                else
                {
                    if (Convert.ToDouble(strPmax) <= Convert.ToDouble(strMaxValue))
                        MaxCheck = true;
                    else
                        MaxCheck = false;
                }
            }
            else
                MaxCheck = false;

            if (strCtlValue.Equals("And"))
            {
                if (MaxCheck && MinCheck)
                    idearPower = true;
            }
            else
            {
                if (MaxCheck || MinCheck)
                    idearPower=true;
            }
            return idearPower;
        }
        	  
		  #region Modification by Jacky on SN format change
           private string GetPONumber(string SN)
        {
        	if (string.IsNullOrEmpty(SN)) {
        		MessageBox.Show("Serial number: "+SN+" does not have a matched PO, please check!");
        		return "";
        	}
        	
        	string sql = @"select po from PO_Modification where InterID = (select MAX(InterID) from PO_Modification where sn = '"+SN+"')";
        	string sequence = "";
        	string Line = "";
        	
        	string syr = SN.Substring(1,2);
        	string sMonth = SN.Substring(3,2);
        	DataTable dt = GetDataTable1(sql);
        	if (dt != null && dt.Rows.Count > 0) {
        		sequence = dt.Rows[0][0].ToString().Substring(6,3);
		        Line = dt.Rows[0][0].ToString().Substring(1,1);
        	}        
        	else
        	{
        		sequence = SN.Substring(7,3);
	        	int lineNumber = Convert.ToInt32(SN.Substring(5,2));
	        
	        
	        	//Changeover on 2016/10/01
	        	if(Convert.ToInt16(syr) > 16 || Convert.ToInt16(sMonth) > 9)
	        	{
		        	switch (lineNumber) {
		        			case 40:
		        					Line = "A";
		        					break;
		        				case 41:
		        					Line = "A";
		        					break;
		        				case 42:
		        					Line = "B";
		        					break;
		        				case 43:
		        					Line = "B";
		        					break;
		        				case 44:
		        					Line = "C";
		        					break;
		        				case 45:
		        					return GetPONumber(getOriginalSn(SN));
		        		}
	        	}
	        	else
	        	{
		        	if(lineNumber > 9 && lineNumber < 20 )
		        		Line = "A";
		        	else if(lineNumber > 19 && lineNumber < 30 )
		        		Line = "B";
		        	else if(lineNumber > 29 && lineNumber < 40 )
		        		Line = "C";
		        	else if(lineNumber > 39 && lineNumber < 50 )
		        		Line = "D";
	        	}
        	}
        	
        	return "C"+Line+syr+sMonth+sequence;
        	
        	
        }
        
        private string getOriginalSn(string SN)
        {
        	string sql = @" select DuplicatedSN from SapDuplicationSN where NewSN = '{0}'";
	        		sql = string.Format(sql,SN);
	        		DataTable OriginalSn = GetDataTable1(sql);
	        		if (OriginalSn != null && OriginalSn.Rows.Count > 0 ) {
	        			return OriginalSn.Rows[0][0].ToString();
	        		}
	        		return "";
        }
        
        
        
         private  DataTable GetDataTable1(string SqlStr, string connection ="")
        {
        	string con = "";
        	DataTable ds = new DataTable();
        	if (string.IsNullOrEmpty( connection ))
        		con = cstring;
            else
            	con = connection;
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(con))
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = SqlStr;
                cmd.CommandType = CommandType.Text;
                //cmd.Connection = conn;
                using (System.Data.SqlClient.SqlDataAdapter ada = new System.Data.SqlClient.SqlDataAdapter())
                {
                    ada.SelectCommand = cmd;
                    try
                    {
                        ada.Fill(ds);
                    }
                    catch (Exception ex)
                    {
                        System.Windows.Forms.MessageBox.Show(ex.Message, "Exception Message", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                    }
                }

            }

            return ds;
        }
         
         #endregion
		
	}
}
