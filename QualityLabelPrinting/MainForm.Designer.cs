﻿/*
 * Created by SharpDevelop.
 * User: shen.yu
 * Date: 3/30/2012
 * Time: 10:10 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace QualityLabelPrinting
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.tbSN = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.tbSN2 = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// tbSN
			// 
			this.tbSN.Location = new System.Drawing.Point(12, 59);
			this.tbSN.Multiline = true;
			this.tbSN.Name = "tbSN";
			this.tbSN.Size = new System.Drawing.Size(147, 370);
			this.tbSN.TabIndex = 1;
			this.tbSN.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TbSNKeyPress);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(12, 41);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(71, 15);
			this.label1.TabIndex = 3;
			this.label1.Text = "Power Label";
			// 
			// tbSN2
			// 
			this.tbSN2.Location = new System.Drawing.Point(179, 59);
			this.tbSN2.Multiline = true;
			this.tbSN2.Name = "tbSN2";
			this.tbSN2.Size = new System.Drawing.Size(147, 370);
			this.tbSN2.TabIndex = 4;
			this.tbSN2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TbSN2KeyPress);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(179, 41);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(69, 12);
			this.label2.TabIndex = 5;
			this.label2.Text = "White Label";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(266, 438);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(60, 13);
			this.label3.TabIndex = 6;
			this.label3.Text = "Version 3.0";
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
			this.label4.Location = new System.Drawing.Point(15, 9);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(314, 25);
			this.label4.TabIndex = 7;
			this.label4.Text = "Manually Print Power Or White Labels";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(338, 461);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.tbSN2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.tbSN);
			this.Name = "MainForm";
			this.Text = "Label Printing";
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox tbSN2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox tbSN;
		
	}
}
